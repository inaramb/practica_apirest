FROM node

WORKDIR /apitechu

ADD . /apitechu

EXPOSE 3000

CMD ["npm", "start"] 
