var express = require('express');
var port = process.env.PORT || 3000;

var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json());

var baseMLabURL = 'https://api.mlab.com/api/1/databases/apitechuiac/collections/';
var mLabAPIKey = "apiKey=gmXmG65PBf_8S2SG_vUTGucQBIi2PsGH";

var requestJson = require('request-json');


app.listen(port);
console.log("##############################")
console.log("API TechU escuchando en el puerto " + port);

app.get("/apitechu/v1",
  function(req, res){
    console.log("##############################")
    console.log("GET /apitechu/v1");
    res.send({"msg" : "Hola desde APITechU"});
  }
)

app.get("/apitechu/v1/users",
  function(req, res){
    console.log("##############################")
    console.log("GET /apitechu/v1/users");
    res.sendFile('usuarios.json', {root:__dirname});
  }
)

app.get("/apitechu/v2/users",
  function(req, res){
    console.log("##############################")
    console.log("GET /apitechu/v2/users");

    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Cliente HTTP creado");

    httpClient.get("user?" + mLabAPIKey,
      function (err, resMLab, body){
        var response = !err ? body : {
          "msg" : "Error obteniendo usuarios"
        }
        res.send(response);
      }
    )
  }
)

app.get("/apitechu/v2/users/:id",
  function(req, res){
    console.log("##############################")
    console.log("GET /apitechu/v2/users/:id");

    var id = req.params.id;
    var query = 'q={"id":' + id + '}';

    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Cliente HTTP creado");

    httpClient.get("user?" + query + "&" + mLabAPIKey,
      function (err, resMLab, body){
        var response = {};
        if (err) {
          response = {
            "msg" : "Error obteniendo usuarios"
          }
          res.status(500);
        } else {
          if (body.length > 0) {
            response = body;
          } else {
            response = {
              "msg" : "Usuario no encontrado"
            };
            res.status(404);
          }
        }
        res.send(response);
      }
    )
  }
)



app.post("/apitechu/v1/users",
  function (req, res){
  console.log("##############################")
    console.log("POST /apitechu/v1/users");
    console.log("first_name: " + req.body.first_name);
    console.log("last_name: " + req.body.last_name);
    console.log("email : " + req.body.email);
    console.log("country: " + req.body.country);
    console.log("gender: " + req.body.gender);
    console.log("password: " + req.body.password);

    var newUser = {
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "email" : req.body.email,
      "gender" : req.body.gender,
      "country" : req.body.country,
      "password" : req.body.password
    };

    var users = require('./usuarios.json');
    users.push(newUser);

    writeUserDataToFile(users);
    msg = "Usuario guardado con exito";
    console.log(msg);
    res.send({"msg" : msg});
  }
)

app.delete("/apitechu/v1/users/:id",
  function (req, res){
    console.log("##############################")
    console.log("DELETE /apitechu/v1/users/:id");
    var id = req.params.id;
    console.log("id: " + id);

    var users = require('./usuarios.json');
    users.splice(id-1, 1);

    writeUserDataToFile(users);
    msg = "Usuario " + id + " borrado con exito.";
    console.log(msg);
    res.send({"msg" : msg});
  }
)

app.post("/apitechu/v1/monstruo/:p1/:p2",
  function (req, res){
    console.log("Parametros");
    console.log(req.params);

    console.log("Query String");
    console.log(req.query);

    console.log("Body");
    console.log(req.body);

    console.log("Headers");
    console.log(req.headers);
  }
)


app.post("/apitechu/v1/login",
  function (req, res){
    console.log("##############################")
    console.log("POST apitechu/v1/login");
    var msg = "Login incorrecto"

    var userEmail = req.body.email;
    var userPassword = req.body.password;

    var users = require('./usuarios.json');
    for (user of users){
      if (user.email == userEmail && user.password == userPassword){
        msg = "Usuario logado con exito"
        console.log(msg);
        user.logged = true
        var userId = user.id;
      }
    }
    writeUserDataToFile(users);
    res.send({"msg" : msg, "id" : userId});
  }
)

app.post("/apitechu/v2/login",
  function (req, res){
    console.log("##############################")
    console.log("POST apitechu/v2/login");
    var msg = "Login incorrecto"

    var userEmail = req.body.email;
    var userPassword = req.body.password;

    var query = 'q={"email": "' + userEmail + '", $and: [ { "password": "' + userPassword + '" } ] }';
    var filter = 'f={"_id":0, "id":1}';

    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Cliente HTTP creado");

    httpClient.get("user?" + query + "&" + filter + "&" + mLabAPIKey,
      function (err, resMLab, body){
        var response = {};
        if (err) {
          response = {
            "msg" : "Error obteniendo usuarios"
          }
          res.status(500);
        } else {
          if (body.length > 0) {
            var putBody = '{"$set":{"logged":true}}';
            httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
              function (errPut, resMLabPut, bodyPut){

              }
            )
            console.log(body[0].id);
            response = {
              "msg" : "Usuario logado con exito",
              "id" : body[0].id
            };
          } else {
            response = {
              "msg" : "Usuario no encontrado"
            };
            res.status(404);
          }
        }
        res.send(response);
      }
    )
  }
)

app.post("/apitechu/v1/logout",
  function (req, res) {
    console.log("##############################")
    console.log("POST apitechu/v1/logout");

    var msg = "usuario no encontrado"

    var userId = req.body.id;
    var users = require('./usuarios.json');

    for (user of users){
      if (user.id == userId){
        if (user.logged){
          msg = "Usuario deslogado"
          console.log(msg);
          delete user.logged;
        } else {
          msg = "Usuario no está logado"
          console.log(msg);
        }

      }
    }
    writeUserDataToFile(users);
    res.send({"msg" : msg});
  }
)

app.post("/apitechu/v2/logout",
  function (req, res) {
    console.log("##############################")
    console.log("POST apitechu/v2/logout");

    var userId = req.body.id;


    var query = 'q={"id": ' + userId + '}';

    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Cliente HTTP creado");

    httpClient.get("user?" + query + "&" + mLabAPIKey,
      function (err, resMLab, body){
        var response = {};
        if (err) {
          response = {
            "msg" : "Error obteniendo usuarios"
          }
          res.status(500);
        } else {
          if (body.length > 0) {
            var userLogged = body[0].logged;
            if (userLogged) {
              var putBody = '{"$unset":{"logged":""}}';
              httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                function (errPut, resMLabPut, bodyPut){

                }
              )
              response = {
                "msg" : "Usuario deslogado"
              };
            } else {
              response = {
                "msg" : "Usuario no está logado"
              };
            }
          } else {
            response = {
              "msg" : "Usuario no encontrado"
            };
            res.status(404);
          }
        }
        res.send(response);
      }
    )
  }
)


function writeUserDataToFile (data){
  var fs = require('fs');
  var jsonData = JSON.stringify(data);

  fs.writeFile("./usuarios.json", jsonData, "utf-8",
    function (err){
      var msg = "No entra en if";
      if (err){
        console.log(err);
      } else {
        console.log("Datos escritos en archivo");
      }
    }
  );
}

app.get("/apitechu/v2/users/:id/accounts",
  function(req, res){
    console.log("##############################")
    console.log("GET /apitechu/v2/users/:id/accounts");

    var id = req.params.id;
    var query = 'q={"userid":' + id + '}';
    var filter = 'f={"_id":0, "iban":1, "balance":1}';

    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Cliente HTTP creado");

    httpClient.get("account?" + query + "&" + filter + "&" + mLabAPIKey,
      function (err, resMLab, body){
        var response = {};
        if (err) {
          response = {
            "msg" : "Error obteniendo cuentas"
          }
          res.status(500);
        } else {
          if (body.length > 0) {
            response = body;
          } else {
            response = {
              "msg" : "El usuario no tiene cuentas asociadas"
            };
            res.status(404);
          }
        }
        res.send(response);
      }
    )
  }
)
